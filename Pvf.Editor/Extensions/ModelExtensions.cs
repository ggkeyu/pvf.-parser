﻿using Pvf.Core;
using Pvf.Editor.Models;
using System;
using System.Windows.Media.Imaging;

namespace Pvf.Editor.Extensions
{
    public static class ModelExtensions
    {
        private static BitmapImage FolderIcon;
        private static BitmapImage FileIcon;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static FileContainerModel CreateFileContainer(string name, IFileContainer fileContainer)
        {
            if (FolderIcon == null)
            {
                FolderIcon = new BitmapImage(new Uri("pack://application:,,,/Pvf.Editor;component/images/folder.png"));
            }

            var folder = new FileContainerModel()
            {
                Icon = FolderIcon,
                Name = name,
                Handle = fileContainer
            };

            if (fileContainer != null)
            {
                var subFolders = fileContainer.GetTopFileContainers();

                foreach (var subFolder in subFolders)
                {
                    var subFolderModel = CreateFileContainer(subFolder.Name, subFolder);

                    folder.Items.Add(subFolderModel);
                }
            }

            return folder;
        }

        public static FileItemModel CreateFile(string name, IFile file)
        {
            if (FileIcon == null)
            {
                FileIcon = new BitmapImage(new Uri("pack://application:,,,/Pvf.Editor;component/images/file.png"));
            }
            return new FileItemModel()
            {
                Icon = FileIcon,
                Name = name,
                Handle = file
            };
        }
    }
}
