﻿using Pvf.Core;
using System.Collections.ObjectModel;

namespace Pvf.Editor.Models
{
    public class FileContainerModel : FileModelBase
    {
        public ObservableCollection<FileModelBase> Items { get; } = new ObservableCollection<FileModelBase>();

        public IFileContainer Handle { get; set; }
    }
}
