﻿using Pvf.Core;

namespace Pvf.Editor.Models
{

    public class FileItemModel : FileModelBase
    {
        public IFile Handle { get; set; }

    }
}
