﻿using System.Windows.Media;

namespace Pvf.Editor.Models
{
    public abstract class FileModelBase : ModelBase
    {
        private string _name;
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private bool _isExpaned;
        /// <summary>
        /// 
        /// </summary>
        public bool IsExpanded
        {
            get => _isExpaned;
            set
            {
                _isExpaned = value;
                OnPropertyChanged(nameof(IsExpanded));
            }
        }

        private ImageSource _icon;
        /// <summary>
        /// 
        /// </summary>
        public ImageSource Icon
        {
            get => _icon;
            set
            {
                _icon = value;
                OnPropertyChanged(nameof(Icon));
            }
        }

    }
}
