﻿namespace Pvf.Core
{
    public sealed class PvfFile : IFile
    {
        public PvfFile(string fileName, IPvfEnv env, PvfIndex data)
        {
            Env = env;
            Name = fileName;
            Data = data;
        }

        /// <summary>F
        /// 
        /// </summary>
        public IPvfEnv Env { get; }

        /// <summary>
        /// 
        /// </summary>
        public IFileContainer Parent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PvfIndex Data { get; }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
