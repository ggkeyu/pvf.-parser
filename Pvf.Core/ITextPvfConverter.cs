﻿using System.IO;

namespace Pvf.Core
{
    /// <summary>
    /// 文本转换器接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITextPvfConverter<out T> : IPvfConverter
    {
        T Deserialize(StreamReader reader, IPvfModule module = null);
    }

    /// <summary>
    /// 文本数据转换器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class PvfTextConverter<T> : PvfConverterBase, ITextPvfConverter<T>
    {
        public abstract T Deserialize(StreamReader reader, IPvfModule module = null);
    }
}
