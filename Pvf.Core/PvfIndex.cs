﻿using System.Text;
using Pvf.Core.Contents;

namespace Pvf.Core
{
    /// <summary>
    /// Pvf 索引
    /// </summary>
    public class PvfIndex
    {
        /// <summary>
        /// 文件ID
        /// </summary>
        public uint FileId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件长度(解密前)
        /// </summary>
        public int FileLength { get; set; }

        /// <summary>
        /// 文件长度(解密后)
        /// </summary>
        public int ComputedFileLength { get; set; }

        /// <summary>
        /// 文件CRC32
        /// </summary>
        public uint FileCrc32 { get; set; }

        /// <summary>
        /// 索引部分后的偏移量
        /// </summary>
        public int RelativeOffset { get; set; }

        /// <summary>
        /// 绝对位置
        /// </summary>
        public int AbsoluteOffset { get; set; }

        public override string ToString()
        {
            return $"[{FileId}]{FileName}";
        }

        /// <summary>
        /// 处理器
        /// </summary>
        public IPvfEnv Env { get; internal set; }

        private PvfContent _content;

        /// <summary>
        /// 加载内容
        /// </summary>
        /// <typeparam name="TContent"></typeparam>
        /// <param name="encoding"></param>
        public void LoadContent<TContent>(Encoding encoding = null, IPvfModule module = null)
                        where TContent : PvfContent, new()
        {
            if (Env == null)
            {
                return;
            }
            if (_content != null)
            {
                return;
            }
            var tCnt = new TContent()
            {
                Index = this
            };

            if (!tCnt.Load(encoding, module))
            {
                return;
            }
            _content = tCnt;
        }

        /// <summary>
        /// 获取内容
        /// </summary>
        /// <typeparam name="TContent"></typeparam>
        /// <param name="encoding"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        public TContent GetContent<TContent>(Encoding encoding = null, IPvfModule module = null)
            where TContent : PvfContent, new()
        {
            LoadContent<TContent>(encoding, module);
            return _content as TContent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TObj"></typeparam>
        /// <param name="encoding"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        public TObj GetObject<TObj>(Encoding encoding = null, IPvfModule module = null)
            where TObj: FObject
        {
            var content = GetContent<PvfScriptObjectContent<TObj>>(encoding:encoding, module: module);
            return content?.Data;
        }
    }
}
