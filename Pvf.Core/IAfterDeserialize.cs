﻿namespace Pvf.Core
{
    public interface IAfterDeserialize<TSrc, TProperty>
    {
        void AfterDeserialize(ref TSrc src, ref TProperty value);
    }
}
