﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pvf.Core.Contents
{
    public sealed class PvfStringTableContent : PvfContent<Dictionary<int, string>>
    {
        private Dictionary<int, string> _table;

        public override Dictionary<int, string> Data => _table;

        public override bool Load(Encoding encoding, IPvfModule module)
        {
            if (_table != null)
            {
                return true;
            }

            if (!base.Load(encoding, module))
            {
                return false;
            }

            if (encoding == null)
            {
                encoding = Encoding.GetEncoding("BIG5");
            }

            _table = new Dictionary<int, string>();

            var data = BytesData;

            var count = BitConverter.ToInt32(data, 0);
            for (int i = 0; i < count; i++)
            {
                int startpos = BitConverter.ToInt32(data, i * 4 + 4);//每次循环的第一个int是键开始的地址
                int endpos = BitConverter.ToInt32(data, i * 4 + 8);//每次循环的第二个int是键结束的地址
                int len = endpos - startpos;//相减就是值的长度

                var pathBytes = new byte[len];//分配内存以存储该值的字符串
                Array.Copy(data, startpos + 4, pathBytes, 0, len);//取出该字符串内容
                string pathName = encoding.GetString(pathBytes).TrimEnd(new char[1]);//解码，这里使用的是BIG5，对于某些文件不一定正确，如果需要更正可以在这个编码这里下手。
                _table.Add(i, pathName.Trim()); //放到索引表中备用
            }

            return true;
        }
    }
}
