﻿using System;
using System.Text;

namespace Pvf.Core.Contents
{

    public sealed class PvfFormattedStringContent : PvfContent<string>
    {
        private string _stringData;

        public override string Data => _stringData;

        public override bool Load(Encoding encoding, IPvfModule module)
        {
            if (!string.IsNullOrEmpty(_stringData))
            {
                return true;
            }
            if (!base.Load(encoding, module))
            {
                return false;
            }

            byte[] unpackedStrBytes = BytesData;

            var sb = new StringBuilder();

            sb.AppendLine("#PVF_File");

            //文件结构为：byte[2](0xb0,0xd0打开好多文件这里都是这个值，猜测应该是固定的)byte[1]int[1]byte[1]int[1]byte[1]...以此循环，byte[1]为指示符，int[1]占四位为一个数字，具体意义需要看指示位
            if (unpackedStrBytes.Length >= 7)//如果总字节长度>=7
            {
                for (int i = 2; i < unpackedStrBytes.Length; i += 5)//以5为单步从第二位开始遍历字节
                {
                    //string s = encoding.GetString(numArray).TrimEnd(new char[1]);
                    if (unpackedStrBytes.Length - i >= 5)//到最后了就不处理了防止内存越界
                    {
                        byte currentByte = unpackedStrBytes[i];//猜测应该是内容指示位
                        if (currentByte == 2 || currentByte == 4 || currentByte == 5 || currentByte == 6 || currentByte == 7 || currentByte == 8 || currentByte == 10)
                        //如果这个字节是这些中的一个进行对应的特殊处理，如果不是那就没有字符串
                        {
                            int after1 = BitConverter.ToInt32(unpackedStrBytes, i + 1);//取该指示位后面的整数
                            if (currentByte == 10)//这个字符是10时
                            {
                                int before1 = BitConverter.ToInt32(unpackedStrBytes, i - 4);//取指示位前面的整数
                                sb.AppendLine(UnpackSpecialChr(currentByte, after1, before1, module));
                            }
                            else if (currentByte == 7)//这个字符是7时
                            {
                                sb.Append("`");
                                sb.Append(UnpackSpecialChr(currentByte, after1, 0));
                                sb.Append("`");
                                sb.AppendLine();
                            }
                            else if (currentByte == 2 || currentByte == 4)//这个字符是2或者4时，末尾不是换行而是制表符\t
                            {
                                sb.Append(UnpackSpecialChr(currentByte, after1, 0));
                                sb.Append("\t");
                            }
                            else if (currentByte == 6 || currentByte == 8)//{指示位=`stringbin[后面的整数]`}
                            {
                                sb.Append("{");
                                sb.Append(currentByte.ToString());
                                sb.Append("=`");
                                sb.Append(UnpackSpecialChr(currentByte, after1, 0));
                                sb.Append("`");
                                sb.Append("}");
                                sb.AppendLine();
                            }
                            else if (currentByte == 5) //是5的情况，stringbin[后面的整数]
                            {
                                var str = UnpackSpecialChr(currentByte, after1, 0);
                                
                                if(str == "[equipment physical defense]")
                                {

                                }
                                sb.AppendLine();
                                sb.Append(str);
                                sb.AppendLine();
                            }
                        }
                    }
                }
                sb.AppendLine();
            }
            _stringData = sb.ToString();//转换成文本
            return true;
        }

        private string UnpackSpecialChr(byte curr, int after1, int before1, IPvfModule module = null)
        {
            switch (curr)
            {
                case 2:
                    return after1.ToString();//int型数
                case 4:
                    return BitConverter.ToSingle(BitConverter.GetBytes(after1), 0).ToString("f6");//4为float型数
                case 5:
                case 6:
                case 7:
                case 8:
                    {

                        return Index.Env.StringTable.TryGetValue(after1, out var foundValue) ? foundValue : "";
                    }
                case 10:
                    {
                        return !Index.Env.StringTable.TryGetValue(after1, out var foundValue) ? "" : $"<{before1}::{foundValue}`{GetNString(foundValue, module)}`>";
                    }
            }
            return "";
        }

        private string GetNString(string str, IPvfModule module)
        {
            if (module != null && module.Strings != null)
            {
                return module.Strings.TryGetValue(str, out var foundValue) ? foundValue : "";
            }
            else
            {
                return Index.Env.NStringTable.TryGetValue(str, out var foundValue) ? foundValue : "";
            }
        }
    }
}
