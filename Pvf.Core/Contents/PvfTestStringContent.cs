﻿using System;
using System.Text;

namespace Pvf.Core.Contents
{
    public sealed class PvfTestStringContent : PvfContent<string>
    {
        private string _stringData;
        public override string Data => _stringData;

        public override bool Load(Encoding encoding, IPvfModule module)
        {
            if (_stringData != null)
            {
                return true;
            }
            if (!base.Load(encoding, module))
            {
                return false;
            }

            byte[] unpackedStrBytes = BytesData;
            var sb = new StringBuilder();

            sb.AppendLine("#PVF_File");

            //文件结构为：byte[2](0xb0,0xd0打开好多文件这里都是这个值，猜测应该是固定的)byte[1]int[1]byte[1]int[1]byte[1]...以此循环，byte[1]为指示符，int[1]占四位为一个数字，具体意义需要看指示位
            if (unpackedStrBytes.Length >= 7)//如果总字节长度>=7
            {
                for (int i = 2; i < unpackedStrBytes.Length; i += 5)//以5为单步从第二位开始遍历字节
                {
                    if (unpackedStrBytes.Length - i >= 5)//到最后了就不处理了防止内存越界
                    {
                        byte currentByte = unpackedStrBytes[i];//猜测应该是内容指示位
                        int after1 = BitConverter.ToInt32(unpackedStrBytes, i + 1);
                        Console.WriteLine($"{currentByte}\t{after1}");
                    }
                }
                sb.AppendLine();
            }
            _stringData = sb.ToString();//转换成文本
            return true;
        }
    }
}
