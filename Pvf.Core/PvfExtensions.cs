﻿using Pvf.Core.Contents;

namespace Pvf.Core
{
    /// <summary>
    /// 
    /// </summary>
    public static class PvfExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TObj"></typeparam>
        /// <param name="module"></param>
        /// <param name="objId"></param>
        /// <returns></returns>
        public static TObj GetObject<TObj>(this IPvfModule module, int objId)
            where TObj : FObject
        {
            var content = module.GetItem(objId).GetRef().GetObject<TObj>(encoding: null, module: module);
            return content;
        }
    }
}
