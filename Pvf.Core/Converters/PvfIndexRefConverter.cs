﻿using System;
using System.IO;

namespace Pvf.Core.Converters
{
    internal sealed class PvfIndexRefConverter : PvfBinaryConverter<PvfIndexRef>
    {
        private IPvfConverter _valueConverter;

        protected override void OnInit()
        {
            _valueConverter = PvfObjectBuilder.Build(typeof(string), Layout.Env);
        }

        public override PvfIndexRef Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            if (_valueConverter is IBinaryPvfConverter<string> binaryPvfConverter)
            {
                var path = binaryPvfConverter.Deserialize(reader, module);

                var item = new PvfIndexRef()
                {
                    File = path,
                    Env = Layout.Env,
                    Module = module
                };
                return item;
            }
            else
            {
                throw new NotSupportedException("不支持的转换器");
            }
        }
    }
}
