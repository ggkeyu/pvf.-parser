﻿using System;

namespace Pvf.Core.Converters
{
    internal static class ConverterStatics
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        /// <param name="enumType"></param>
        /// <param name="baseType"></param>
        /// <param name="fallbackImplType"></param>
        /// <returns></returns>
        public static IPvfConverter MakeDynamicConverter(IPvfEnv env, Type enumType, Type baseType, Type fallbackImplType)
        {
            var gType = typeof(DynamicElementConverter<,,>).MakeGenericType(enumType, baseType, fallbackImplType);
            var converter = Activator.CreateInstance(gType) as IPvfConverter;
            if (converter != null)
            {
                converter.Init(new PvfObjectLayout(baseType, env));
            }
            return converter;
        }
    }
}
