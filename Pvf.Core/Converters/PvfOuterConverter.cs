﻿using System;
using System.IO;

namespace Pvf.Core.Converters
{
    public class PvfOuterConverter<TObj> : PvfBinaryConverter<TObj>
    {
        /// <inheritdoc />
        protected override void OnInit()
        {
            base.OnInit();
        }

        /// <inheritdoc />
        public override TObj Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            throw new NotImplementedException();
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class PvfOuterAttribute : Attribute
    {
        /// <summary>
        /// PvfObject类
        /// </summary>
        public Type ObjType { get; }
        
        /// <summary>
        /// 
        /// </summary>
        public string[] Fields { get; }

        public PvfOuterAttribute(Type objType, string[] fields)
        {
            ObjType = objType;
            Fields = fields;
        }
    }
}