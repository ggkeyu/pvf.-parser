﻿using System.IO;

namespace Pvf.Core.Converters
{
    internal sealed class PvfInt32Converter : PvfBinaryConverter<int>
    {
        public override bool CanRead(BinaryReader reader)
        {
            var peek = reader.PeekChar();
            return peek == 2;
        }

        public override int Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            TypeCheck(reader, 2);

            return reader.ReadInt32();
        }
    }
}
