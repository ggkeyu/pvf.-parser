﻿using System.Collections.Generic;

namespace Pvf.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFileContainer : IFile
    {
        /// <summary>
        /// 所有子文件
        /// </summary>
        IList<IFile> Items { get; }

        /// <summary>
        /// 添加
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file"></param>
        /// <returns></returns>
        T Add<T>(T file) where T : IFile;

        /// <summary>
        /// 添加
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        T Get<T>(string name) where T : IFile;

        /// <summary>
        /// 排序
        /// </summary>
        /// <param name="desc"></param>
        void Sort(bool desc = false);

        /// <summary>
        /// 获取顶层文件夹
        /// </summary>
        /// <returns></returns>
        string[] GetTopFileContainersName();

        /// <summary>
        /// 获取顶层文件夹
        /// </summary>
        /// <returns></returns>
        IFileContainer[] GetTopFileContainers();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string[] GetTopFilesName();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IFile[] GetTopFiles();
    }
}
