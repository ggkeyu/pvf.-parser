﻿using System.IO;

namespace Pvf.Core
{
    /// <summary>
    /// 二进制转换器接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBinaryPvfConverter<out T> : IPvfConverter
    {
        T Deserialize(BinaryReader reader, IPvfModule module = null);
    }


    /// <summary>
    /// 二进制数据转换器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class PvfBinaryConverter<T> : PvfConverterBase, IBinaryPvfConverter<T>
    {
        public abstract T Deserialize(BinaryReader reader, IPvfModule module = null);
    }
}
