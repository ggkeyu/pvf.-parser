﻿using System.IO;

namespace Pvf.Core
{
    /// <summary>
    /// 转换器接口
    /// </summary>
    public interface IPvfConverter
    {
        /// <summary>
        /// 
        /// </summary>
        IPvfObjectLayout Layout { get; }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="layout"></param>
        void Init(IPvfObjectLayout layout);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        bool CanRead(BinaryReader reader);
    }
}
