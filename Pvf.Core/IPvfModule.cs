﻿using System.Collections.Generic;

namespace Pvf.Core
{
    /// <summary>
    /// Pvf模块
    /// </summary>
    public interface IPvfModule 
    {

        /// <summary>
        /// 
        /// </summary>
        string Name { get; }

        /// <summary>
        /// lst文件数据
        /// </summary>
        PvfIndex Index { get; }

        /// <summary>
        /// 对象
        /// </summary>
        IDictionary<int, PvfIndexRef> Objects { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<string, string> Strings { get; }

        /// <summary>
        /// 组合文件夹
        /// </summary>
        /// <param name="subFile"></param>
        /// <returns></returns>
        string CombinePath(string subFile);

        /// <summary>
        /// 获取物品
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        PvfIndexRef GetItem(int key);
    }
}
