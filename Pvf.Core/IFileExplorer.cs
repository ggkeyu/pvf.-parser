﻿using System.Collections.Generic;

namespace Pvf.Core
{
    /// <summary>
    /// 文件管理器
    /// </summary>
    public interface IFileExplorer
    {
        /// <summary>
        /// Pvf环境
        /// </summary>
        IPvfEnv Env { get; }

        /// <summary>
        /// 添加文件/文件夹
        /// </summary>
        /// <param name="addTo"></param>
        /// <param name="file"></param>
        void Add(string addTo, IFile file);

        /// <summary>
        /// 遍历
        /// </summary>
        /// <param name="action"></param>
        void Foreach(System.Action<IFile> action);

        /// <summary>
        /// 查找文件/文件夹
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="convertSplitChar"></param>
        /// <returns></returns>
        T Find<T>(string path, bool convertSplitChar) where T : IFile;

        /// <summary>
        /// 获取顶层文件夹
        /// </summary>
        /// <returns></returns>
        string[] GetTopFileContainersName();

        /// <summary>
        /// 获取顶层文件夹
        /// </summary>
        /// <returns></returns>
        IFileContainer[] GetTopFileContainers();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string[] GetTopFilesName();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IFile[] GetTopFiles();
    }
}
