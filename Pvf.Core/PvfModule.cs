﻿using Pvf.Core.Contents;
using System.Collections.Generic;
using System.Text;

namespace Pvf.Core
{

    /// <summary>
    /// Pvf模块实现类
    /// </summary>
    public abstract class PvfModule : IPvfModule, IPvfModuleInitializer
    {
        private readonly IPvfEnv _env;

        private PvfObjectContent<Dictionary<int, PvfIndexRef>> _indexContent;

        private PvfDictContent _strContent;

        /// <summary>
        /// lst文件编码
        /// </summary>
        public Encoding Encoding { get; }

        /// <summary>
        /// 
        /// </summary>
        public PvfIndex Index { get; private set; }

        /// <summary>
        /// 对象
        /// </summary>
        public IDictionary<int, PvfIndexRef> Objects => _indexContent?.Data;

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<string, string> Strings => _strContent?.Data;

        /// <summary>
        /// 文件夹
        /// </summary>
        public string Name { get; private set; }

        protected PvfModule(IPvfEnv env, string name, Encoding encoding)
        {
            _env = env;

            Encoding = encoding;

            Name = name;
        }

        private void LoadStr(IFileContainer modFolder)
        {
            if (string.IsNullOrEmpty(_env.NLang))
            {
                return;
            }

            var index = modFolder.Get<PvfFile>($"{Name}.{_env.NLang}.str")?.Data;

            if (index == null)
            {
                return;
            }
            _strContent = index.GetContent<PvfDictContent>(Encoding.GetEncoding("BIG5"), this);
        }

        private void LoadIndex(IFileContainer modFolder)
        {
            Index = modFolder.Get<PvfFile>($"{Name}.lst")?.Data;

            if (Index == null) return;

            _indexContent = Index.GetContent<PvfObjectContent<Dictionary<int, PvfIndexRef>>>(Encoding, this);

            if(_indexContent == null || _indexContent.Data == null)
            {
                return;
            }

            foreach (var item in _indexContent.Data)
            {
                item.Value.Index = item.Key;
                item.Value.File = item.Value.File.Replace('\\', '/');
            }
        }

        public void Init()
        {
            var modFolder = _env.FileExplorer.Find<IFileContainer>(Name, false);

            if (modFolder == null)
            {
                OnInit();

                return;
            }

            LoadIndex(modFolder);

            LoadStr(modFolder);

            OnInit();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        protected virtual void OnInit()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subFile"></param>
        /// <returns></returns>
        public virtual string CombinePath(string subFile)
        {
            return $"{Name}/{subFile}";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual PvfIndexRef GetItem(int key)
        {
            return Objects != null && Objects.TryGetValue(key, out var foundRef) ? foundRef : null;
        }
    }
}
