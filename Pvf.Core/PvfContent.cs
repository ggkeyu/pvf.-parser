﻿using System;
using System.Text;

namespace Pvf.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class PvfContent
    {
        public PvfIndex Index { get; set; }

        public virtual bool Load(Encoding encoding, IPvfModule module)
        {
            if (BytesData != null)
            {
                return true;
            }
            if (Index.Env == null)
            {
                return false;
            }

            BytesData = Index.Env.UnpackContent(Index.AbsoluteOffset, Index.ComputedFileLength, Index.FileCrc32);
            for (int i = 0; i < Index.ComputedFileLength - Index.FileLength; i++)
            {
                BytesData[Index.FileLength + i] = 0;
            }

            return true;
        }

        /// <summary>
        /// 字节内容
        /// </summary>
        public byte[] BytesData { get; private set; }

        /// <summary>
        /// 释放内存
        /// </summary>
        public void GC()
        {
            BytesData = Array.Empty<byte>();

            System.GC.Collect();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public abstract class PvfContent<TData> : PvfContent
    {
        public abstract TData Data { get; }
    }
}
