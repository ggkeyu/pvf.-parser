﻿namespace Pvf.Core
{
    /// <summary>
    /// Pvf对象
    /// </summary>
    public abstract class PvfObject : PvfObjectBase
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 名字2
        /// </summary>
        public string Name2 { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}::{Name}";
        }
    }
}