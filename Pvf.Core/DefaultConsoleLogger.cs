﻿using System;

namespace Pvf.Core
{
    public sealed class DefaultConsoleLogger : ILogger
    {
        public void LogError(string type, string msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{type}::{msg}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LogException(string type, Exception exception)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{type}::{exception.Message}\r\n{exception.StackTrace}");
            Console.ForegroundColor = ConsoleColor.White;

        }

        public void LogInfo(string type, string msg)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"{type}::{msg}");
        }

        public void LogWarning(string type, string msg)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"{type}::{msg}");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
