﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Pvf.Core
{
    internal class PvfObjectLayout : IPvfObjectLayout
    {
        public Type ObjType { get; }

        public List<int> Keys { get; } = new List<int>();

        public List<int> ClosedKeys { get; } = new List<int>();

        public IPvfEnv Env { get; }

        public PropertyInfo BaseProperty { get; set; }

        public bool IsRequired { get; set; }

        public bool IsEmpty { get; set; }

        public bool IsDynamic { get; set; }

        public PvfObjectLayout(Type objType, IPvfEnv env)
        {
            ObjType = objType;
            Env = env;
        }
    }
}
