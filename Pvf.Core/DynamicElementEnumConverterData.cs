﻿namespace Pvf.Core
{
    internal struct DynamicElementEnumConverterData<TEnum>:IDynamicElementEnumConverterData
    {
        public TEnum EnumValue { get; set; }

        public IPvfConverter Converter { get; set; }
    }
}
