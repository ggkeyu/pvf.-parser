﻿namespace Pvf.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFile
    {
        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        IFileContainer Parent { get; set; }
    }
}
