﻿namespace Pvf.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDynamicElementEntity
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEnumKey"></typeparam>
    public interface IDynamicElementEntity<TEnumKey> : IDynamicElementEntity
        where TEnumKey : struct
    {
        TEnumKey Key { get; set; }
    }
}
