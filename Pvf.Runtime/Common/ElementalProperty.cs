﻿using Pvf.Core;
using System;

namespace Pvf.Runtime.Common
{
    /// <summary>
    /// 元素属性
    /// </summary>
    [Flags]
    public enum ElementalProperty
    {
        /// <summary>
        /// 无属性
        /// </summary>
        [PvfIngore]
        NoElement = 0,
        /// <summary>
        /// 光属性
        /// </summary>
        [PvfFields(true, "light")]
        LightElement = 1,
        /// <summary>
        /// 暗属性
        /// </summary>
        DarkElement = 2,
        /// <summary>
        /// 火属性
        /// </summary>
        FireElement = 4,
        /// <summary>
        /// 冰属性
        /// </summary>
        WaterElement = 8,
    }

}
