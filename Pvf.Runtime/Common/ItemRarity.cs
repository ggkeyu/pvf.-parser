﻿namespace Pvf.Runtime.Common
{
    /// <summary>
    /// 物品稀有度
    /// </summary>
    public enum ItemRarity
    {
        /// <summary>
        /// 白色 普通
        /// </summary>
        Common = 0,
        /// <summary>
        /// 蓝色 高级
        /// </summary>
        Advanced = 1,
        /// <summary>
        /// 紫色 稀有
        /// </summary>
        Rare = 2,
        /// <summary>
        /// 粉色,神器
        /// </summary>
        Unique = 3,
        /// <summary>
        /// 红色 勇者
        /// </summary>
        Chronicle = 4,
        /// <summary>
        /// 橘色 传送
        /// </summary>
        Legend = 5,
        /// <summary>
        /// 金色 史诗
        /// </summary>
        Epic = 6,
        /// <summary>
        /// 神话
        /// </summary>
        Mythology
    }

}
