﻿using Pvf.Core;

namespace Pvf.Runtime.Common
{
    /// <summary>
    /// 拥有最大最小值的属性
    /// </summary>
    [PvfRequired]
    public class RangedAttachValue : IAttachValue
    {
        [PvfIngore]
        public AttributeDefine Key { get; set; }

        public float MinValue { get; set; }

        public float MaxValue { get; set; }

        /// <summary>
        /// 获取长度
        /// </summary>
        /// <returns></returns>
        public float GetLength()
        {
            return MaxValue - MinValue;
        }

        /// <summary>
        /// 获取数值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public float GetValue(float a)
        {
            return MinValue + a * (MaxValue - MinValue);
        }

        /// <summary>
        /// 默认值
        /// </summary>
        [PvfDefaultValue]
        public static RangedAttachValue Empty { get; } = new RangedAttachValue()
        {
            MinValue = 0,
            MaxValue = 0
        };

        public override string ToString()
        {
            return $"{Key}::{MinValue:F2}-{MaxValue:F2}";
        }
    }

}
