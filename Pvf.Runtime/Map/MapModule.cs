﻿using Pvf.Core;
using System.Text;

namespace Pvf.Runtime.Map
{
    [ModuleEntry("map")]
    public sealed class MapModule : PvfModule
    {
        public MapModule(IPvfEnv env, string name, Encoding encoding) : base(env, name, encoding)
        {
        }
    }
}
