﻿using Pvf.Core;
using System.Text;

namespace Pvf.Runtime.Equipment
{
    /// <summary>
    /// 
    /// </summary>
    [ModuleEntry("equipment")]
    public sealed class EquipmentModule : PvfModule
    {
        public EquipmentModule(IPvfEnv env, string name, Encoding encoding) : base(env, name, encoding)
        {
        }
    }
}
