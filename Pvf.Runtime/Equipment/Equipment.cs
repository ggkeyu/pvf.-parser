﻿using Pvf.Core;
using Pvf.Runtime.Common;
using System.Collections.Generic;
using Pvf.Core.Converters;

namespace Pvf.Runtime.Equipment
{
    /// <summary>
    /// 装备
    /// </summary>
    public class Equipment : PvfObject
    {
        /// <summary>
        /// 描述
        /// </summary>
        public string Explain { get; set; }

        /// <summary>
        /// 基础描述
        /// </summary>
        public string BasicExplain { get; set; }

        /// <summary>
        /// 详细描述
        /// </summary>
        public string DetailExplain { get; set; }

        /// <summary>
        /// 灰色字体
        /// </summary>
        public string FlavorText { get; set; }

        /// <summary>
        /// 掉落等级?
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 负重
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// 售价
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// NPC销售价格，无[value]时以此价格的5分之一作为出售价格
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// 修理价格
        /// </summary>
        public float RepairPrice { get; set; }

        /// <summary>
        /// 耐久度
        /// </summary>
        public int Durability { get; set; }

        /// <summary>
        /// 最小使用等级
        /// </summary>
        public int MinimumLevel { get; set; }

        /// <summary>
        /// 最大使用等级
        /// </summary>
        public int MaximumLevel { get; set; }

        /// <summary>
        /// 武器类型
        /// </summary>
        public string ItemGroupName { get; set; }

        /// <summary>
        /// 装备类型
        /// </summary>
        public EquipmentTypeInfo EquipmentType { get; set; }

        /// <summary>
        /// 可使用职业
        /// </summary>
        [PvfClosed]
        public List<CharacterClass> UsableJob { get; internal set; }

        /// <summary>
        /// 品质
        /// </summary>
        public ItemRarity Rarity { get; set; }

        /// <summary>
        /// 子类型
        /// </summary>
        public int SubType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MoveWav { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CoolTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [PvfEmpty]
        public bool PossibleKiriProtect { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public PvfImage Icon { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PvfImage FieldImage { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public AttachType AttachType { get; set; }

        /// <summary>
        /// 属性值
        /// </summary>
        [PvfDynamicElement(typeof(AttributeDefine), typeof(AttachValue))]
        public List<IAttachValue> AttachValues { get; internal set; }

        /// <summary>
        /// 元素
        /// </summary>
        public ElementalProperty ElementalProperty { get; set; }

        /// <summary>
        /// 隐藏的装备类型
        /// </summary>
        [PvfClosed]
        public List<EquipmentType> HideEquipment { get; set; }

        /// <summary>
        /// 隐藏的图层
        /// </summary>
        [PvfClosed]
        public List<int> HideLayer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [PvfEmpty]
        public bool FullAvatar { get; set; }
        
        /// <summary>
        /// 所属套装
        /// </summary>
        [PvfIngore]
        [PvfOuter(typeof(EquipmentSet), new string[]{"set name","set item","set ability"})]
        public IEquipmentSet SetOwner { get; set; }
        
        /// <summary>
        /// 套装所属id
        /// </summary>
        public int PartSetIndex { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public List<int> SetItem { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string SetName { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //[PvfObjectSet]
        //public List<SkillDataUp> SkillDataUp { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //[PvfObjectSet]
        //public List<SkillLevelUp> SkillLevelUp { get; set; }
        
        public Equipment()
        {

        }
    }

}
