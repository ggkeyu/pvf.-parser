﻿using Pvf.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pvf.Runtime.Region
{
    [ModuleEntry("region")]
    public sealed class RegionModule : PvfModule
    {
        public RegionModule(IPvfEnv env, string name, Encoding encoding) : base(env, name, encoding)
        {
        }
    }
}
